<header class="masthead" style="background-image: url('/public/images/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>BeeJee Tasks</h1>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
<?php if (empty($list)): ?>
     <p>No Tasks Now</p>
<?php else: ?>
    <table class="table">
        <tr class="sort" data-type="DESC">
            <th data-sort="u.name">User</th>
            <th data-sort="u.email">Email</th>
            <th data-sort="t.description">Description</th>
            <th data-sort="s.status_name">Status</th>
        </tr>
        <?php foreach ($list as $val): ?>
           <tr>
                <td><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></td>
                <td><?php echo htmlspecialchars($val['email'], ENT_QUOTES); ?></td>
                <td><?php echo ($val['admin_edit'] == 1) ? '<b>Admin edit</b> ' : ''; ?><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></td>
                <td><?php echo htmlspecialchars($val['status_name'], ENT_QUOTES); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<div class="clearfix">
    <?php echo $pagination; ?>
</div>
<?php endif; ?>
</div>