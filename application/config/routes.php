<?php

return [
	// MainController
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],
	'main/index/{page:\d+}' => [
		'controller' => 'main',
		'action' => 'index',
	],
	'sort' => [
		'controller' => 'main',
		'action' => 'sort',
	],
	'sort/index/{page:\d+}' => [
		'controller' => 'main',
		'action' => 'sort',
	],
	'tasks' => [
		'controller' => 'main',
		'action' => 'tasks',
	],	
	// AdminController
	'admin/login' => [
		'controller' => 'admin',
		'action' => 'login',
	],
	'admin/logout' => [
		'controller' => 'admin',
		'action' => 'logout',
	],	
	'admin/edit/{id:\d+}' => [
		'controller' => 'admin',
		'action' => 'edit',
	],
	'admin/delete/{id:\d+}' => [
		'controller' => 'admin',
		'action' => 'delete',
	],	
	'admin/tasks' => [
		'controller' => 'admin',
		'action' => 'tasks',
	],	
];