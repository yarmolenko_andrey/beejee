<?php

return [
	'all' => [
		'index',
		'tasks',
		'sort',
		'add'
	],
	'authorize' => [		
	],
	'guest' => [
	],
	'admin' => [
	],
];