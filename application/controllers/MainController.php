<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\models\Admin;

class MainController extends Controller {

	public function indexAction() {
		$sort_type = (isset($_POST['sort-type'])) ? $_POST['sort-type'] : 'DESC';
		$sort_by = (isset($_POST['sort-by'])) ? lcfirst($_POST['sort-by']) : 't.id';
		$page = (isset($_POST['page'])) ? $_POST['page'] : $this->route;	
		$pagination = new Pagination($this->route, $this->model->tasksCount());
		$vars = [
			'pagination' => $pagination->get(),
			'list' => $this->model->tasksList($page, $sort_type, $sort_by),
		];
		$this->view->render('BeeJee Tasks', $vars);
	}

	public function sortAction() {
		$sort_type = (isset($_POST['sort-type'])) ? $_POST['sort-type'] : 'DESC';
		$sort_by = (isset($_POST['sort-by'])) ? lcfirst($_POST['sort-by']) : 't.id';
		$page = (isset($_POST['page'])) ? $_POST['page'] : $this->route;	
		$pagination = new Pagination($this->route, $this->model->tasksCount());
		$vars = [
			'pagination' => $pagination->get(),
			'list' => $this->model->tasksList($page, $sort_type, $sort_by),
		];
		echo json_encode($vars);
	}	

	public function tasksAction() {
		if (!empty($_POST)) {
			if (!$this->model->taskValidate($_POST)) {
				$this->view->message('error', $this->model->error);
			}
			$user_id = $this->model->getUserIdByEmail($_POST);
			
			if (!$user_id) {
				$user_id = $this->model->addUser($_POST);
			}

			$this->view->message('success', 'Task creates');
			$user_id = $this->model->getUserIdByEmail($_POST); 
			
			if (!$user_id) {
				$this->model->addUser($_POST);
			}
			
			if(empty($user_id)) {

				$user['name'] = $_POST['name'];
				$user['email'] = $_POST['email'];
				$this->model->addUser($user);
			};

			$this->view->message('success', 'its created');
		}
		$this->view->render('Add new task');
	}
}