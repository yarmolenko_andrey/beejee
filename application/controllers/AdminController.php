<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\models\Main;

class AdminController extends Controller {

	public function __construct($route) {
		parent::__construct($route);
		$this->view->layout = 'admin';
	}

	public function loginAction() {
		if (isset($_SESSION['admin'])) {
			$this->view->redirect('admin/tasks');
		}
		if (!empty($_POST)) {
			if (!$this->model->loginValidate($_POST)) {
				$this->view->message('error', $this->model->error);
			}
			$_SESSION['admin'] = true;
			$this->view->location('admin/tasks');
		}
		$this->view->render('Login');
	}

	public function tasksAction() {
		$mainModel = new Main;
		$pagination = new Pagination($this->route, $mainModel->tasksCount());
		$vars = [
			'pagination' => $pagination->get(),
			'list' => $mainModel->tasksList($this->route),
		];
		$this->view->render('Tasks', $vars);
	}

	// public function addAction() {
	// 	if (!empty($_POST)) {
	// 		if (!$this->model->postValidate($_POST, 'add')) {
	// 			$this->view->message('error', $this->model->error);
	// 		}
	// 		$id = $this->model->postAdd($_POST);
	// 		if (!$id) {
	// 			$this->view->message('success', 'Ошибка обработки запроса');
	// 		}
	// 		$this->model->postUploadImage($_FILES['img']['tmp_name'], $id);
	// 		$this->view->message('success', 'Пост добавлен');
	// 	}
	// 	$this->view->render('Добавить пост');
	// }

	public function editAction() {
		$admin_edit = 0;
		if (!$this->model->isTaskExists($this->route['id'])) {
			$this->view->errorCode(404);
		} 
		else {
			$task_description = $this->model->getTaskDescription($this->route['id']);
		}
		if (!empty($_POST)) {
			if (!$this->model->taskValidate($_POST, 'edit')) {
				$this->view->message('error', $this->model->error);
			}
			if($_POST['description'] !== $task_description) {
				$admin_edit = 1;
			}
			$_POST['admin_edit'] = $admin_edit;
			$this->model->taskEdit($_POST, $this->route['id']);
			$this->view->message('success', 'Task Saved');
			$this->view->redirect('admin/tasks');
		}
		$vars = [
			'data' => $this->model->taskData($this->route['id'])[0],
			'status' => $this->model->getAllStatus(),
		];		
		$this->view->render('Edit task', $vars);
	}	

	public function deleteAction() {
		if (!$this->model->isTaskExists($this->route['id'])) {
			$this->view->errorCode(404);
		}
		$this->model->taskDelete($this->route['id']);
		$this->view->redirect('admin/tasks');
	}

	public function logoutAction() {
		unset($_SESSION['admin']);
		$this->view->redirect('admin/login');
	}	
}