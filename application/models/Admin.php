<?php

namespace application\models;

use application\core\Model;

class Admin extends Model {

	public $error;

	public function loginValidate($post) {
		$config = require 'application/config/admin.php';

		if ($config['login'] != $post['login'] or $config['password'] != $post['password']) {
			$this->error = 'Incorrect Login or Pswd';
			return false;
		}

		return true;
	}

	public function postValidate($post, $type) {
		$nameLen = iconv_strlen($post['name']);
		$descriptionLen = iconv_strlen($post['description']);
		$textLen = iconv_strlen($post['text']);

		if ($nameLen < 3 or $nameLen > 100) {
			$this->error = 'Name needs containt from 3 to 100 symbols';
			return false;
		} elseif ($descriptionLen < 3 or $descriptionLen > 1000) {
			$this->error = 'Description needs containt from 3 to 1000 symbols';
			return false;
		} 

		return true;
	}

	public function taskValidate($post, $type) {
		$descriptionLen = iconv_strlen($post['description']);

		if ($descriptionLen < 3 or $descriptionLen > 1000) {
			$this->error = 'Description needs containt from 3 to 1000 symbols';
			return false;
		} 
		
		return true;
	} 

	public function postAdd($post) {
		$params = [
			'id' => '',
			'name' => $post['name'],
			'description' => $post['description'],
			'text' => $post['text'],
		];
		$this->db->query('INSERT INTO posts VALUES (:id, :name, :description, :text)', $params);

		return $this->db->lastInsertId();
	}

	public function tasksAdd($post) {
		$params = [
			'id' => '',
			'name' => $post['name'],
			'description' => $post['description'],
			'email' => $post['email'],
			'status_id' => 1,
		];
		$this->db->query('INSERT INTO tasks VALUES (:id, :name, :description, :text)', $params);

		return $this->db->lastInsertId();
	}

	public function taskEdit($post, $id) {
		$params = [
			'id' => $id,			
			'description' => $post['description'],
			'status_id' => $post['status_id'],
			'admin_edit' => $post['admin_edit'],
		];
		$this->db->query('UPDATE tasks SET description = :description, status_id = :status_id, admin_edit = :admin_edit WHERE id = :id', $params);
	}

	public function getTaskDescription($id) {
		$params = [
			'id' => $id,			
		];

		return $this->db->column('SELECT description FROM tasks WHERE id = :id', $params);
	}

	public function isTaskExists($id) {
		$params = [
			'id' => $id,
		];

		return $this->db->column('SELECT id FROM tasks WHERE id = :id', $params);
	}
	
	public function taskDelete($id) {
		$params = [
			'id' => $id,
		];
		$this->db->query('DELETE FROM tasks WHERE id = :id', $params);
	}

	public function taskData($id) {
		$params = [
			'id' => $id,
		];
		
		return $this->db->row('SELECT t.id, t.description, u.name, s.status_name, s.id AS status_id, t.admin_edit AS admin_edit FROM tasks AS t JOIN users AS u ON t.user_id = u.id JOIN status AS s ON t.status_id = s.id WHERE t.id = :id', $params);
	}

	public function getAllStatus() {
		
		return $this->db->row('SELECT * FROM status');
	}
}