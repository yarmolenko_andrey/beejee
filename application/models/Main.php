<?php

namespace application\models;

use application\core\Model;

class Main extends Model {

	public $error;

	public function taskValidate($post) {
		$nameLen = iconv_strlen($post['name']);
		$textLen = iconv_strlen($post['description']);
		if ($nameLen < 3 or $nameLen > 20) {
			$this->error = 'Name needs containt from 3 to 20 symbols';
			return false;
		} elseif (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error = 'Incorrect E-mail';
			return false;
		} elseif ($textLen < 10 or $textLen > 1000) {
			$this->error = 'Description needs containt from 3 to 1000 symbols';
			return false;
		}
		return true;
	}

	public function tasksCount() {
		return $this->db->column('SELECT COUNT(id) FROM tasks');
	}

	public function tasksList($route, $sort_type, $sort_by) {
		$max = 3;
		$route_page = isset($route['page']) ? $route['page'] : 1;
		$params = [
			'max' => $max,
			'start' => (($route_page - 1) * $max),
			'type' => $sort_type,
			'by' => $sort_by,
		];

		return $this->db->row('SELECT t.id, t.description, u.name, u.email, s.status_name, t.admin_edit AS admin_edit FROM tasks AS t JOIN users AS u ON t.user_id = u.id JOIN status AS s ON t.status_id = s.id ORDER BY :by :type LIMIT :start, :max', $params);
	}

	public function getUserIdByEmail($post) {
		$params = [
			'email' => $post['email']
		];
		
		return $this->db->column('SELECT id FROM users WHERE email = :email',$params);
	}

	public function addUser($post) {
		$params = [
			'id' => '',
			'name' => $post['name'],
			'pswd' => '1234',
			'email' => $post['email'],
			'role_id' => 0,
		];

		$user =  $this->db->query('INSERT INTO users VALUES (:id, :name, :pswd, :email, :role_id)', $params);
				
		return $this->db->lastInsertId();		
	}	

	public function taskAdd($post) {
		$params = [
			'id' => '',
			'description' => $post['description'],
			'status_id' => $post['status_id'],
			'user_id' => $post['user_id']
			'admin_edit' => 0,
		];
		$this->db->query('INSERT INTO tasks (:id, :description, :status_id, :user_id, :admin_edit)', $params);
		
		return $this->db->lastInsertId();
	}

}