$(document).ready(function() {
	$('form').submit(function(event) {
		var json;
		event.preventDefault();
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData: false,
			success: function(result) {
				json = jQuery.parseJSON(result);
				if (json.url) {
					window.location.href = '/' + json.url;
				} else {
					alert(json.status + ' - ' + json.message);
				}
			},
		});
	});

	$('tr.sort th').on('click', function(e) {
		e.preventDefault();
		var sort = $('tr.sort'),
			page = 0,
			url = $(location).attr('href').substr(-1);
		
		if(sort.data('type') == 'DESC') {
			sort.data('type','ASC');
		} else {
			sort.data('type','DESC');
		}
		if ( !isNaN(url) ) {
		  page = url;
		}		

		$.ajax({
	        type: 'POST',
	        url: '/sort',
	        dataType: 'json',
	        data: '&page=' + page + '&sort-type=' + sort.data('type') + '&sort-by=' + $(this).attr('data-sort'),
			success: function(json) {
    			if (json['list']) {
    				$('.row-table').remove();
    				$($div).insertAfter('tr.sort'); 
    				var $div = '',
    					admin_edit = '';
    				for (var i = 0; i < json['list'].length; i++) {
    					if (json['list'][i]['admin_edit'] == 1) {
    						admin_edit = '<b>Admin edit</b>';
    					}

    					$div += '<tr class="row-table">';
    						$div += '<td>' + json['list'][i]['name'] + '</td>';
    						$div += '<td>' + json['list'][i]['email'] + '</td>';
                        	$div += '<td>' + admin_edit + ' ' +  json['list'][i]['description'] + '</td>';                        	
                        	$div += '<td>' + json['list'][i]['status_name'] + '</td>';                                                            
                    	$div += '</tr>';
    				}
    				$($div).insertAfter('tr.sort');    				
    			}
        	},
        	error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});	
	});
});